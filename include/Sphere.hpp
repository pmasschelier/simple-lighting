#ifndef SPHERE_HPP
#define SPHERE_HPP

#include "OGLWorld.hpp"

class Sphere {

public: 
    static void computeVertices(unsigned short meridians, unsigned short parallels, std::vector<float>& vertices, std::vector<unsigned int>& elements);
};

#endif