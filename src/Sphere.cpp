#include "Sphere.hpp"

void Sphere::computeVertices(unsigned short meridians, unsigned short parallels, std::vector<float>& vertices, std::vector<unsigned int>& elements)
{
    if(parallels < 1)
        throw std::runtime_error("Pour une sphère le nombre de paralleles doit être supérieur à 1.");
    
    const unsigned int nb_vertices(3*2 + 3*parallels*meridians);
    vertices.resize(nb_vertices);

    const unsigned int nb_elements(6*parallels*meridians);
    elements.resize(nb_elements);

    unsigned int vindex(0), eindex(0);

    // Point haut
    vertices[vindex++] = 0.0;
    vertices[vindex++] = 1.0;
    vertices[vindex++] = 0.0;

    // Indices du haut au premier parallèle
    unsigned i(0);
    for(; i + 1 < meridians; i++) {
        elements[eindex++] = 0;
        elements[eindex++] = i+1;
        elements[eindex++] = i+2;
    }

    elements[eindex++] = 0;
    elements[eindex++] = i+1;
    elements[eindex++] = 1;

    double theta, phi;
    float y, radius; // hauteur et rayon de chaque section de parallele
    unsigned offset; // Premier indice de chaque parallele
    float x, z;

    for(unsigned i(0); i < parallels; i++) {
        theta = M_PI * (i + 1) / (parallels + 1);
        y = std::cos(theta);
        radius = std::sin(theta);

        offset = 1 + i * meridians;

        for(unsigned j(0); j < meridians; j++) {
            phi = 2 * M_PI * j / meridians;
            x = std::sin(phi) * radius;
            z = std::cos(phi) * radius;
            
            vertices[vindex++] = x;
            vertices[vindex++] = y;
            vertices[vindex++] = z;

            if(i + 1 == parallels) {
                elements[eindex++] = offset + j;
                elements[eindex++] = offset + meridians;
                if(j + 1 == meridians)
                    elements[eindex++] = offset;
                else
                    elements[eindex++] = offset + j + 1;
            }
            else if(j + 1 == meridians) {
                elements[eindex++] = offset + j;
                elements[eindex++] = offset + j + meridians;
                elements[eindex++] = offset;
                elements[eindex++] = offset;
                elements[eindex++] = offset + j + meridians;
                elements[eindex++] = offset + meridians;
            }
            else {
                elements[eindex++] = offset + j;
                elements[eindex++] = offset + j + meridians;
                elements[eindex++] = offset + j + 1;
                elements[eindex++] = offset + j + 1;
                elements[eindex++] = offset + j + meridians;
                elements[eindex++] = offset + j + meridians + 1;
            }
        }
    }

    // Point bas
    vertices[vindex++] = 0.0;
    vertices[vindex++] = -1.0;
    vertices[vindex++] = 0.0;
}