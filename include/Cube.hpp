#ifndef CUBE_HPP
#define CUBE_HPP

#include "OGLWorld.hpp"

class Cube {

public:

/* ordre : +x, -x, +y, -y, +z, -z, le premier triangle est toujours celui situé le plus vers -x, -y 
 *  les triangles sont tracés sans passer par l'hypothénus et dans le sens trigo.
 *  on a touours un triangle en bas à droite et un triangle en bas à gauche
 */

	static constexpr std::array<float, 108> CUBE = 
	{
		1.0, -1.0, 1.0,     1.0, -1.0, -1.0,     1.0, 1.0, -1.0,		// Face gauche (+x)
		1.0, 1.0, -1.0,		1.0, 1.0, 1.0,       1.0, -1.0, 1.0,		// Face gauche (+x)

		-1.0, -1.0, -1.0,	-1.0, -1.0, 1.0,	-1.0, 1.0, 1.0,			// Face droite (-x)
		-1.0, 1.0, 1.0,		-1.0, 1.0, -1.0,    -1.0, -1.0, -1.0, 		// Face droite (-x)

		1.0, 1.0, -1.0,     -1.0, 1.0, -1.0,     -1.0, 1.0, 1.0, 		// Face dessus (+y)
		-1.0, 1.0, 1.0,		1.0, 1.0, 1.0,		1.0, 1.0, -1.0,			// Face dessus (+y)

		-1.0, -1.0, 1.0,   -1.0, -1.0, -1.0,	1.0, -1.0, -1.0,		// Face dessous (-y)
		1.0, -1.0, -1.0,	1.0, -1.0, 1.0,     -1.0, -1.0, 1.0,	    // Face dessous (-y)

		-1.0, -1.0, 1.0,	1.0, -1.0, 1.0,		1.0, 1.0, 1.0,			// Face arrière (+z)
		1.0, 1.0, 1.0,		-1.0, 1.0, 1.0,     -1.0, -1.0, 1.0,  		// Face arrière (+z)

		1.0, -1.0, -1.0,	-1.0, -1.0, -1.0,   -1.0, 1.0, -1.0,        // Face avant (-z)
		-1.0, 1.0, -1.0,    1.0, 1.0, -1.0,     1.0, -1.0, -1.0         // Face avant (-z)
	};


	static constexpr std::array<float, 72> VERTICES =
	{
		1.0, -1.0, 1.0,     1.0, -1.0, -1.0,     1.0, 1.0, -1.0,	1.0, 1.0, 1.0,		// Face gauche (+x)
		-1.0, -1.0, -1.0,	-1.0, -1.0, 1.0,	-1.0, 1.0, 1.0,		-1.0, 1.0, -1.0,	// Face droite (-x)
		1.0, 1.0, -1.0,     -1.0, 1.0, -1.0,     -1.0, 1.0, 1.0,	1.0, 1.0, 1.0, 		// Face dessus (+y)
		-1.0, -1.0, 1.0,   -1.0, -1.0, -1.0,	1.0, -1.0, -1.0,	1.0, -1.0, 1.0,		// Face dessous (-y)
		-1.0, -1.0, 1.0,	1.0, -1.0, 1.0,		1.0, 1.0, 1.0,		-1.0, 1.0, 1.0,		// Face arrière (+z)
		1.0, -1.0, -1.0,	-1.0, -1.0, -1.0,   -1.0, 1.0, -1.0,	1.0, 1.0, -1.0		// Face avant (-z)
	};

	static constexpr std::array<float, 72> NORMALS =
	{
    	1., 0., 0., 	1., 0., 0., 	1., 0., 0., 	1., 0., 0., 	// Face gauche (+x)
		-1., 0., 0.,	-1., 0., 0.,	-1., 0., 0.,	-1., 0., 0.,	// Face droite (-x)
		0., 1., 0.,		0., 1., 0.,		0., 1., 0.,		0., 1., 0.,		// Face dessus (+y)
		0., -1., 0.,	0., -1., 0.,	0., -1., 0.,	0., -1., 0.,	// Face dessous (-y)
		0., 0., 1.,		0., 0., 1.,		0., 0., 1.,		0., 0., 1.,		// Face arrière (+z)
		0., 0., -1.,	0., 0., -1.,	0., 0., -1.,	0., 0., -1.		// Face avant (-z)
	};

	static constexpr std::array<unsigned char, 36> INDICES =
	{
		0, 1, 2,		2, 3, 0,
		4, 5, 6,		6, 7, 4,
		8, 9, 10,		10, 11, 8,
		12, 13, 14,		14, 15, 12,
		16, 17, 18,		18, 19, 16,
		20, 21, 22,		22, 23, 20
	};

	class VBO {
	public:
		VBO(GLenum type = GL_ARRAY_BUFFER);
		~VBO();

		template <typename T, std::size_t N>
		void load(std::array<T, N> data);
		bool isLoaded() const;
		GLuint getID() const;

	private:
		GLenum m_type;
		GLuint id;
		bool loaded;
	};

private:

	static VBO cubeVBO;
	static VBO verticesBO;
	static VBO normalsBO;
	static VBO elementsBO;


public:

	static GLuint getCubeVBO();
	static GLuint getVerticesBO();
	static GLuint getNormalsBO();
	static GLuint getElementsBO();

	static std::array<float, 108> computeColorsByFace(std::array<sf::Color, 6> colors);

	static std::array<float, 72> computeTexCoords(unsigned width, unsigned height, std::array<unsigned, 6> const& index);

/*
public:

	Cube();

	void setTexCoords(std::array<float, 72> const& texCoords);

	glm::mat4 modelMatrix;

private:

	GLuint m_VAO;
	
	std::shared_ptr<sf::Texture>  m_tex;
	std::shared_ptr<sf::Shader> m_shader;*/

};

#endif