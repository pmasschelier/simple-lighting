#include "Scene.hpp"


OGLScene::OGLScene(std::shared_ptr<sf::RenderWindow> window) : 
	m_window(window),
	m_light(glm::vec3(0, 2, 0)),
	m_camera(
		glm::vec3(-24, 18, -18), // Position de la caméra
		M_PI/4, // horizontalAngle
		-M_PI/5, // verticalAngle
		45.f // FoV
	),
	m_skytexture(std::make_shared<CubemapTexture>())
{
	m_camera.setRatio((float)m_window->getSize().x / (float) m_window->getSize().y);

	m_window->setActive(true);

	if (!font.loadFromFile("ressources/arial.ttf"))
	{
		std::cerr << "Erreur lors du chargement de la police." << std::endl;
	}

	glEnable(GL_DEPTH_TEST);
	// Accepte le fragment s'il est plus proche de la caméra que l'ancien
	glDepthFunc(GL_LESS);
	glEnable(GL_CULL_FACE); // Par défaut : glCullFace(GL_CCW);
	
	loadModels();
	loadMaterials();
}

OGLScene::~OGLScene()
{
	glDeleteBuffers(1, &m_sphereEB);
	glDeleteBuffers(1, &m_sphereVB);

	glDeleteVertexArrays(1, &m_sphereVAO);

	glDeleteVertexArrays(1, &m_cubeVAO);
}

void OGLScene::loadMaterials()
{
	std::ifstream file("ressources/materials.csv");
	std::stringstream sstr;
	std::string str;

	if(file) {
		std::getline(file, str);
		while(!file.eof()) {
			Material m;
			char c;
			std::stringstream name;
			while(file.get(c) && c != '\t')
				name << c;
			m.name = name.str();
			file >> m.ambiant.x >> m.ambiant.y >> m.ambiant.z;
			file >> m.diffuse.x >> m.diffuse.y >> m.diffuse.z;
			file >> m.specular.x >> m.specular.y >> m.specular.z;
			file >> m.shininess;
			m.shininess *= 128.f;
#ifdef DEBUG
			std::cout << m.name << std::endl;
			std::cout << m.ambiant.x << " - " << m.ambiant.y << " - " << m.ambiant.z << std::endl;
			std::cout << m.diffuse.x << " - " << m.diffuse.y << " - " << m.diffuse.z << std::endl;
			std::cout << m.specular.x << " - " << m.specular.y << " - " << m.specular.z << std::endl;
			std::cout << m.shininess << std::endl;
#endif
			m_materials.push_back(m);
		}
	}
	m_currentMaterial = m_materials.begin();
}

void OGLScene::loadModels() 
{
	m_cubeVB = Cube::getVerticesBO();
	m_cubeNB = Cube::getNormalsBO();
	m_cubeEB = Cube::getElementsBO();
#ifdef DEBUG
	std::cout << "Vertex buffer du cube : " << m_cubeVB << std::endl;
#endif

	// Destruction d'un éventuel ancien VAO
    if(glIsVertexArray(m_cubeVAO) == GL_TRUE)
		glDeleteVertexArrays(1, &m_cubeVAO);

	// Génération d'un VAO
	glGenVertexArrays(1, &m_cubeVAO);

	// Remplissage du VAO
	glBindVertexArray(m_cubeVAO);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_cubeEB);

		glBindBuffer(GL_ARRAY_BUFFER, m_cubeVB);
		glVertexAttribPointer(
			0,                  // Numéro de l'attribut (doit correspondre dans le shader)
			3,                  // taille du tableau
			GL_FLOAT,           // type des éléments
			GL_FALSE,           // normalized?
			0,                  // stride, 0 => les éléments sont considérés comme juxtaposés dans le tableau
			(void*)0            // offset
			);
		glEnableVertexAttribArray(0);

		glBindBuffer(GL_ARRAY_BUFFER, m_cubeNB);
		glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);
		glEnableVertexAttribArray(1);

		glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);

	m_cubeModels[0] = glm::translate(glm::mat4(1.0), glm::vec3(-5.0f, 0.0f, -5.0f));
	m_cubeModels[1] = glm::translate(glm::mat4(1.0), glm::vec3(5.0f, 0.0f, -5.0f));
	m_cubeModels[2] = glm::translate(glm::mat4(1.0), glm::vec3(-5.0f, 0.0f, 5.0f));
	m_cubeModels[3] = glm::translate(glm::mat4(1.0), glm::vec3(5.0f, 0.0f, 5.0f));

	// VAO de la sphere
	
    Sphere::computeVertices(100, 100, sphere_vertices, sphere_elements);

	if(glIsBuffer(m_sphereVB) == GL_TRUE)
        glDeleteBuffers(1, &m_sphereVB);
    glGenBuffers(1, &m_sphereVB);
	glBindBuffer(GL_ARRAY_BUFFER, m_sphereVB);
	glBufferData(GL_ARRAY_BUFFER, sphere_vertices.size() * sizeof(float), &sphere_vertices[0], GL_STATIC_DRAW);

	if(glIsBuffer(m_sphereEB) == GL_TRUE)
        glDeleteBuffers(1, &m_sphereEB);
    glGenBuffers(1, &m_sphereEB);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_sphereEB);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sphere_elements.size() * sizeof(unsigned int), &sphere_elements[0], GL_STATIC_DRAW);

	if(glIsVertexArray(m_sphereVAO) == GL_TRUE)
		glDeleteVertexArrays(1, &m_sphereVAO);
	glGenVertexArrays(1, &m_sphereVAO);

	glBindVertexArray(m_sphereVAO);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_sphereEB);

		glBindBuffer(GL_ARRAY_BUFFER, m_sphereVB);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);
		glEnableVertexAttribArray(0);

	glBindVertexArray(0);

	m_sphereModel = glm::mat4(1.0);
}

void OGLScene::draw() const
{

	// Dessine les 4 cubes

	glUseProgram(m_cubeShader.getID());
	
	glBindVertexArray(m_cubeVAO);

		m_cubeShader.setMat4("viewProjectionMatrix", m_camera.getViewProjectionMatrix());
		m_cubeShader.setVec3("cameraPos", m_camera.getPosition());

		m_cubeShader.setVec3("material.ambient", m_currentMaterial->ambiant);
		m_cubeShader.setVec3("material.diffuse", m_currentMaterial->diffuse);
		m_cubeShader.setVec3("material.specular", m_currentMaterial->specular);
		m_cubeShader.setFloat("material.shininess", m_currentMaterial->shininess);

		m_cubeShader.setLight("light", m_light);

		for(auto& mat : m_cubeModels) {
			
			m_cubeShader.setMat4("model", mat);

			glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_BYTE, 0); // Commence au sommet 0; 6 faces au total -> 12 triangles
		}

	// Dévérouillage du VAO
	glBindVertexArray(0);

	// Dessine la sphère

	glUseProgram(m_sphereShader.getID());
	
	glBindVertexArray(m_sphereVAO);

		m_sphereShader.setMat4("viewProjectionMatrix", m_camera.getViewProjectionMatrix());
		m_sphereShader.setMat4("model", m_sphereModel);
		m_sphereShader.setVec3("cameraPos", m_camera.getPosition());

		m_sphereShader.setVec3("material.ambient", m_currentMaterial->ambiant);
		m_sphereShader.setVec3("material.diffuse", m_currentMaterial->diffuse);
		m_sphereShader.setVec3("material.specular", m_currentMaterial->specular);
		m_sphereShader.setFloat("material.shininess", m_currentMaterial->shininess);

		m_sphereShader.setLight("light", m_light);

		glDrawElements(GL_TRIANGLES, sphere_elements.size(), GL_UNSIGNED_INT, 0); // Commence au sommet 0; 6 faces au total -> 12 triangles

	glBindVertexArray(0);
	
	// Dessine la lumière

	glUseProgram(m_sparkleShader.getID());

	m_sparkleShader.setMat4("viewProjectionMatrix", m_camera.getViewProjectionMatrix());
	m_sparkleShader.setVec3("cameraUp", m_camera.getUp());
	m_sparkleShader.setVec3("cameraRight", m_camera.getRight());

	
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, glm::value_ptr(m_light.getPosition()));

	sf::Texture::bind(&m_lightTex);

	glEnableVertexAttribArray(0);
	glDrawArrays(GL_POINTS, 0, 1);
	glDisableVertexAttribArray(0);

	m_skybox.render(m_camera);
}

void OGLScene::play()
{
    Input in(m_window, true);

	std::string shadersDir("ressources/shaders/");
	
	m_lightTex.loadFromFile("ressources/sparkle.png");
	m_lightTex.generateMipmap();
	m_sparkleShader.addFile(shadersDir + "sparkle.vert", GL_VERTEX_SHADER);
	m_sparkleShader.addFile(shadersDir + "sparkle.geo", GL_GEOMETRY_SHADER);
	m_sparkleShader.addFile(shadersDir + "sparkle.frag", GL_FRAGMENT_SHADER);
	m_sparkleShader.load();

	m_cubeShader.addFile(shadersDir + "cube.vert", GL_VERTEX_SHADER);
	m_cubeShader.addFile(shadersDir + "lighting.frag", GL_FRAGMENT_SHADER);
	m_cubeShader.load();
	m_sphereShader.addFile(shadersDir + "sphere.vert", GL_VERTEX_SHADER);
	m_sphereShader.addFile(shadersDir + "lighting.frag", GL_FRAGMENT_SHADER);
	m_sphereShader.load();

	std::array<std::string, 6> filenames = {
		"ressources/skybox/skybox_px.jpg",
		"ressources/skybox/skybox_nx.jpg",
		"ressources/skybox/skybox_py.jpg",
		"ressources/skybox/skybox_ny.jpg",
		"ressources/skybox/skybox_pz.jpg",
		"ressources/skybox/skybox_nz.jpg",
	};
	m_skytexture->loadFromFiles(filenames);
	m_skybox.setCubemapTexture(m_skytexture);

	sf::Text text;
	text.setFont(font); // font is a sf::Font
	text.setString(m_currentMaterial->name);
	text.setCharacterSize(24);
	text.setFillColor(sf::Color::Red);
	text.setStyle(sf::Text::Bold | sf::Text::Underlined);
	text.setPosition(50.f, 20.f);

	bool wireframe(false), paused(false);
	sf::Clock loopclock;
	sf::Time lastTime(loopclock.getElapsedTime()), newTime, elapsedTime;

    while (!in.end())
    {
        in.update();

		if(in.appSizeChanged()) {
			float xw = (float)in.appSize().x, yw = (float) in.appSize().y;
			m_camera.setViewport(0.f, 0.f, xw, yw);
			m_window->setView(sf::View(sf::FloatRect(0.f, 0.f, xw, yw)));
		}

		if(in.getKey(sf::Keyboard::W)) {
			if(!wireframe)
				glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
			else
				glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
			wireframe = !wireframe;
			in.setKey(sf::Keyboard::W, false);
		}

		if(in.getKey(sf::Keyboard::P)) {
			in.setRelativeMouse(paused);
			paused = !paused;
			in.setKey(sf::Keyboard::P, false);
		}

		if(in.getKey(sf::Keyboard::Space)) {
			if(++m_currentMaterial == m_materials.end())
				m_currentMaterial = m_materials.begin();
			text.setString(m_currentMaterial->name);
			in.setKey(sf::Keyboard::Space, false);
		}

		if(!paused) {
			newTime = loopclock.getElapsedTime();
			elapsedTime = newTime - lastTime;

			m_camera.update(in, elapsedTime);
			m_light.setPosition({5 * std::cos(M_PI * 0.1 * newTime.asSeconds()), 2, 5 * std::sin(M_PI * 0.1 * newTime.asSeconds())});

			lastTime = newTime;
		}
        
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		
		draw();

		m_window->pushGLStates();
		m_window->draw(text);
		m_window->popGLStates();

		m_window->display();

        sf::sleep(sf::milliseconds(25));
    }
}
