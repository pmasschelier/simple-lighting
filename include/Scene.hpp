#ifndef SCENE_HPP
#define SCENE_HPP

#include "OGLWorld.hpp"
#include "Camera.hpp"
#include "Shader.hpp"
#include "Cube.hpp"
#include "Skybox.hpp"
#include "Sphere.hpp"


struct Material {
	std::string name;
    glm::vec3 ambiant;
    glm::vec3 diffuse;
    glm::vec3 specular;
    float shininess;
};


class OGLScene {
public:
	OGLScene(std::shared_ptr<sf::RenderWindow> window);
	~OGLScene();
	void play();

private:

	void loadMaterials();
	void loadModels();
	void draw() const;
	
private:
	sf::ContextSettings settings;
	std::shared_ptr<sf::RenderWindow> m_window;

	GLuint m_cubeVAO; // Indice du VAO
	GLuint m_cubeEB;
	GLuint m_cubeVB; // Indice du vertex buffer de notre cube
	GLuint m_cubeNB;
	std::array<glm::mat4, 4> m_cubeModels;

	Shader m_cubeShader;


	std::vector<float> sphere_vertices;
	std::vector<unsigned> sphere_elements;
	GLuint m_sphereVAO;
	GLuint m_sphereEB;
	GLuint m_sphereVB; // Indice du vertex buffer de notre sphere
	glm::mat4 m_sphereModel;

	Shader m_sphereShader;

	Light m_light;
	sf::Texture m_lightTex;
	Shader m_sparkleShader;

	std::vector<Material> m_materials;
	std::vector<Material>::iterator m_currentMaterial;

	Camera m_camera;

	sf::Texture m_tex;

	Skybox m_skybox;
	std::shared_ptr<CubemapTexture> m_skytexture;

	sf::Font font;
};

#endif
