#version 330 core
layout(location = 0) in vec3 position;

uniform mat4 viewProjectionMatrix;
uniform mat4 model;

out vec3 posWorld;
out vec3 normalWorld;

void main() {
    normalWorld = normalize(mat3(transpose(inverse(model))) * position);
    posWorld = vec3(model * vec4(position, 1.0));
    gl_Position = viewProjectionMatrix * vec4(posWorld, 1.0); // w = 1 -> position, w = 0 -> direction
}