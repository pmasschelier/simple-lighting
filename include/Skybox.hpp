#ifndef SKYBOX_HPP
#define SKYBOX_HPP

#include "CubemapTexture.hpp"
#include "Camera.hpp"
#include "Shader.hpp"
#include "Cube.hpp"

class Skybox
{
public:
    Skybox();

    void setCubemapTexture(std::shared_ptr<CubemapTexture> cubemapTex);

    bool loadFromFiles(std::array<std::string, 6> const& filenames);

    void render(Camera const& camera) const;

private:
    std::shared_ptr<CubemapTexture> m_cubemapTex;

    GLuint m_vertexbuffer;

    static Shader shader;
};

#endif