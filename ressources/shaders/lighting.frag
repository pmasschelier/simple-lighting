#version 330 core

in vec3 posWorld;
in vec3 normalWorld;

struct Light {
    vec3 position;
  
    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
};

uniform Light light;

struct Material {
    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
    float shininess;
}; 
  
uniform Material material;

uniform vec3 cameraPos;

out vec3 color;

void main() {
    vec3 lightDirection = normalize(light.position - posWorld);
    vec3 viewDirection = normalize(posWorld - cameraPos);
    vec3 reflectionDirection = reflect(-lightDirection, normalWorld);
    float cosTheta = max(dot(lightDirection, normalWorld), 0.0);
    
    vec3 ambient = light.ambient * material.ambient;

    vec3 diffuse = light.diffuse * (cosTheta * material.diffuse);

    float specularStrength = 0.5;
    float spec = pow(max(- dot(viewDirection, reflectionDirection), 0.0), material.shininess);
    vec3 specular = light.specular * (spec * material.specular);

    color = ambient + diffuse + specular;
}