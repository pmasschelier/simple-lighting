#ifndef CUBEMAP_TEXTURE_HPP
#define CUBEMAP_TEXTURE_HPP

#include "OGLWorld.hpp"

class CubemapTexture
{
public:

    CubemapTexture();

    ~CubemapTexture();

    bool loadFromFiles(std::array<std::string, 6> const& filenames);

    bool loadFromTileMap(unsigned widthTex, unsigned heightTex, std::array<unsigned, 6> const& index, std::string const& filename);

    void setSmooth(bool smooth);
    
    bool isSmooth() const;

    void generateMipmap();

    void bind(GLenum TextureUnit = GL_TEXTURE0);

private:

    void create();

    GLuint m_id;

    bool m_smooth;
    bool m_hasMipmap;
};

#endif