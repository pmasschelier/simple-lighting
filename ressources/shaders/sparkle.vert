#version 330 core
layout(location = 0) in vec3 position;

uniform mat4 viewProjectionMatrix;

void main() {
    gl_Position = vec4(position, 1.0); // w = 1 -> position, w = 0 -> direction
}