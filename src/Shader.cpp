#include "Shader.hpp"

static const std::array<GLenum, 6> gl_shader_types = {
	GL_VERTEX_SHADER,
	GL_TESS_CONTROL_SHADER,
	GL_TESS_EVALUATION_SHADER,
	GL_GEOMETRY_SHADER,
	GL_FRAGMENT_SHADER,
	GL_COMPUTE_SHADER
};

static std::string loadFile(std::string const& path)
{
    std::string code;
    std::ifstream file(path, std::ios::in);
    std::stringstream sstr;
    
    if(file.is_open()) {
		sstr << file.rdbuf();
		code = sstr.str();
		file.close();
    }
    else{
        sstr << "Impossible d'ouvrir " << path << ", vérifiez que vous êtes dans le bon dossier." << std::endl;
        throw std::runtime_error(sstr.str());
	}

    return code;
}

Shader::Shader() : m_ready(false)
{

}

Shader::~Shader()
{

	for(shader_t shader : m_shaders)
		glDeleteShader(shader.id);
	
	if(glIsProgram(m_id) == GL_TRUE)
    	glDeleteProgram(m_id);
}

GLuint Shader::compile(GLenum shaderType, std::string const& source)
{
    GLuint shaderID = glCreateShader(shaderType);
    const char* sourcePointer = source.c_str(); // Variable nécessaire parce qu'on ne peut pas récupérer l'adresse d'une lvalue
	glShaderSource(shaderID, 1, &sourcePointer , NULL); // shader id, count, string, length 
	glCompileShader(shaderID);

	// Vérifie le vertex shader
    GLint Result = GL_FALSE;
	int InfoLogLength;

	glGetShaderiv(shaderID, GL_COMPILE_STATUS, &Result);
	glGetShaderiv(shaderID, GL_INFO_LOG_LENGTH, &InfoLogLength);

	if (InfoLogLength > 0) {
		std::vector<char> shaderErrorMessage(InfoLogLength+1);
		glGetShaderInfoLog(shaderID, InfoLogLength, NULL, &shaderErrorMessage[0]);
        std::cerr << &shaderErrorMessage[0] << std::endl;
	}

    return shaderID;
}

void Shader::addFile(std::string const& filename, GLenum shaderType) {
	std::string source = loadFile(filename);

	addSource(source, shaderType, filename);
}

void Shader::addSource(std::string const& source, GLenum shaderType, std::string const& name)
{
	if(std::find(gl_shader_types.begin(), gl_shader_types.end(), shaderType) == gl_shader_types.end()) {
        std::stringstream sstr;
		sstr << "Type de shader inconnu pour  " << name << std::endl;
        throw std::runtime_error(sstr.str());
	}

#ifdef DEBUG
	std::cout << "Compile le shader : " << filename << std::endl;
#endif
	
	shader_t shader;
	shader.filename = name;
	shader.id = compile(shaderType, source);
	shader.type = shaderType;

	m_shaders.push_back(shader);
}

void Shader::load()
{
	if(glIsProgram(m_id) == GL_TRUE)
    	glDeleteProgram(m_id);

	m_id = glCreateProgram();

	// Link le programme
#ifdef DEBUG
	std::cout << "Linkage du shader" << std::endl;
#endif

	for(shader_t shader : m_shaders)
		glAttachShader(m_id, shader.id);

	glLinkProgram(m_id);

	// Vérifie le programme
    GLint Result = GL_FALSE;
	int InfoLogLength;

	glGetProgramiv(m_id, GL_LINK_STATUS, &Result);
	glGetProgramiv(m_id, GL_INFO_LOG_LENGTH, &InfoLogLength);

	if (InfoLogLength > 0){
		std::vector<char> ProgramErrorMessage(InfoLogLength+1);
		glGetProgramInfoLog(m_id, InfoLogLength, NULL, &ProgramErrorMessage[0]);
        std::cerr << &ProgramErrorMessage[0] << std::endl;
	}
	
	for(shader_t shader : m_shaders)
		glDetachShader(m_id, shader.id);

	m_ready = true;
}

bool Shader::ready() const {
	return m_ready;
}

void Shader::setBool(const std::string &name, bool value) const
{         
    glUniform1i(glGetUniformLocation(m_id, name.c_str()), (int)value); 
}

void Shader::setInt(const std::string &name, int value) const
{ 
    glUniform1i(glGetUniformLocation(m_id, name.c_str()), value); 
}

void Shader::setFloat(const std::string &name, float value) const
{ 
    glUniform1f(glGetUniformLocation(m_id, name.c_str()), value); 
}

void Shader::setVec3(const std::string &name, glm::vec3 vec) const
{ 
    glUniform3f(glGetUniformLocation(m_id, name.c_str()), vec.x, vec.y, vec.z); 
}

void Shader::setMat4(const std::string &name, glm::mat4 mat) const
{
	glUniformMatrix4fv(glGetUniformLocation(m_id, name.c_str()), 1, GL_FALSE, glm::value_ptr(mat));
}

void Shader::setLight(const std::string &name, Light const& light) const
{
	auto[ambiant, diffuse, specular] = light.getProperties();
	glUniform3f(glGetUniformLocation(m_id, (name+".position").c_str()), light.getPosition().x, light.getPosition().y, light.getPosition().z);
	glUniform3f(glGetUniformLocation(m_id, (name+".ambiant").c_str()), ambiant.x, ambiant.y, ambiant.z);
	glUniform3f(glGetUniformLocation(m_id, (name+".diffuse").c_str()), diffuse.x, diffuse.y, diffuse.z);
	glUniform3f(glGetUniformLocation(m_id, (name+".specular").c_str()), specular.x, specular.y, specular.z);
}