#version 330 core
layout(location = 0) in vec3 position;
layout(location = 1) in vec3 normal;

uniform mat4 viewProjectionMatrix;
uniform mat4 model;

out vec3 normalWorld;
out vec3 posWorld;

void main() {
    normalWorld = normalize(mat3(transpose(inverse(model))) * normal);
    posWorld = vec3(model * vec4(position, 1.0));
    gl_Position = viewProjectionMatrix * vec4(posWorld, 1.0); // w = 1 -> position, w = 0 -> direction
}