#version 330

layout (points) in;
layout (triangle_strip) out;
layout (max_vertices = 4) out;

uniform mat4 viewProjectionMatrix;
uniform vec3 cameraUp;
uniform vec3 cameraRight;

out vec2 TexCoord;

void main()
{
    vec3 position = gl_in[0].gl_Position.xyz;
    
    position -= (cameraRight * 0.5);
    gl_Position = viewProjectionMatrix * vec4(position, 1.0);
    TexCoord = vec2(0.0, 0.0);
    EmitVertex();

    position += cameraRight;
    gl_Position = viewProjectionMatrix * vec4(position, 1.0);
    TexCoord = vec2(1.0, 0.0);
    EmitVertex();

    position -= cameraRight;
    position += cameraUp;
    gl_Position = viewProjectionMatrix * vec4(position, 1.0);
    TexCoord = vec2(0.0, 1.0);
    EmitVertex();

    position += cameraRight;
    gl_Position = viewProjectionMatrix * vec4(position, 1.0);
    TexCoord = vec2(1.0, 1.0);
    EmitVertex();

    EndPrimitive();
} 