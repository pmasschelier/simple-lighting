# Installation

```
git clone git@framagit.org:pmasschelier/simple-lighting.git
cd simple-lighting
```
Pour compiler et lancer le programme
```
make
./sfmlgl
```

# Interface

Appuyer sur espace pour changer de matériau.

![](ressources/gitlab/emerald.png "Emerald")
![](ressources/gitlab/chrome.png "Chrome")
![](ressources/gitlab/rubber.png "Yellow rubber")

