#include "Skybox.hpp"

Shader Skybox::shader;

static const std::string vertexShader = R"(
#version 330

layout (location = 0) in vec3 position;

uniform mat4 mvp;

out vec3 texCoord0;

void main()
{
    vec4 MVPpos = mvp * vec4(position, 1.0);
    gl_Position = MVPpos.xyww; // On fait z=1 ainsi la skybox perd tous les tests de profondeur
    texCoord0 = position;
};
)";

static const std::string fragmentShader = R"(
#version 330

in vec3 texCoord0;

out vec4 FragColor;

uniform samplerCube cubeTexture;

void main()
{
    FragColor = texture(cubeTexture, texCoord0);
};
)";

Skybox::Skybox() : m_cubemapTex(std::make_shared<CubemapTexture>())
{
    if(!shader.ready()) {
        shader.addSource(vertexShader, GL_VERTEX_SHADER);
        shader.addSource(fragmentShader, GL_FRAGMENT_SHADER);
        shader.load();
    }

    m_vertexbuffer = Cube::getCubeVBO();
}

void Skybox::setCubemapTexture(std::shared_ptr<CubemapTexture> cubemapTex)
{
    m_cubemapTex = cubemapTex;
}

bool Skybox::loadFromFiles(std::array<std::string, 6> const& filenames)
{
    bool result = m_cubemapTex->loadFromFiles(filenames);
    if(result) {
        m_cubemapTex->setSmooth(true);
        return true;
    }
    return false;
}

void Skybox::render(Camera const& camera) const
{
    // Utilise le shader
	glUseProgram(shader.getID());


    glDepthFunc(GL_LEQUAL);
    glCullFace(GL_FRONT); 

    glEnableVertexAttribArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, m_vertexbuffer);
	glVertexAttribPointer(
		0,                  // Numéro de l'attribut (doit correspondre dans le shader)
		3,                  // taille du vecteur
		GL_FLOAT,           // type des éléments
		GL_FALSE,           // normalized?
		0,                  // stride, 0 => les éléments sont considérés comme juxtaposés dans le tableau
		(void*)0            // offset
		);

	// Récupère un "handle" pour la matrice mvp
	GLuint matrixID = glGetUniformLocation(shader.getID(), "mvp");

    glm::mat4 view = glm::mat4(glm::mat3(camera.getViewMatrix()));
    glm::mat4 mvp = camera.getProjectionMatrix() * view;
	
	glUniformMatrix4fv(matrixID, 1, GL_FALSE, glm::value_ptr(mvp));

    m_cubemapTex->bind(GL_TEXTURE0);

	// Dessine le triangle
	glDrawArrays(GL_TRIANGLES, 0, 3*12); // Commence au sommet 0; 6 faces au total -> 12 triangles

	glDisableVertexAttribArray(0);

    glCullFace(GL_BACK);
    glDepthFunc(GL_LESS);

    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glUseProgram(0);
}